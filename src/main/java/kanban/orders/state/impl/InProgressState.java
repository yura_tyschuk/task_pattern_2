package kanban.orders.state.impl;

import kanban.orders.State;
import kanban.orders.Task;

public class InProgressState implements State {


  @Override
  public void addTask(Task task) {
    task.addTaskToMap();
    task.setState(new NewProjectState());

  }

  @Override
  public void addToDo(Task task) {
    task.setState(new ToDoState());
    logger.warn("task moved to do");
  }

  @Override
  public void addToCodeReview(Task task) {
    task.setState(new CodeReviewState());
    logger.warn("task moved to code review state");
  }
}
