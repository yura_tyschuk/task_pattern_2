package kanban.orders.state.impl;

import kanban.orders.State;
import kanban.orders.Task;

public class NewProjectState implements State {

  @Override
  public void addToDo(Task task) {
    task.setState(new ToDoState());
    logger.warn("Task moved to do");
  }

  @Override
  public void addTask(Task task) {
    task.addTaskToMap();
  }
}
