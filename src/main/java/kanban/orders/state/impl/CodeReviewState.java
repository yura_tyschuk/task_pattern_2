package kanban.orders.state.impl;

import kanban.orders.State;
import kanban.orders.Task;

public class CodeReviewState implements State {

  @Override
  public void addToInProgress(Task task) {
    task.setState(new InProgressState());
    logger.warn("task moved to in progress state");
  }

  @Override
  public void addToDone(Task task) {
    task.setState(new DoneState());
    logger.warn("task moved to done");
  }
}
