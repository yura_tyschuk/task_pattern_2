package kanban.orders.state.impl;

import kanban.orders.State;
import kanban.orders.Task;

public class ToDoState implements State {

  @Override
  public void addTask(Task task) {
    task.addTaskToMap();
    task.setState(new NewProjectState());
  }

  @Override
  public void addToInProgress(Task task) {
    task.setState(new InProgressState());
    logger.warn("Task moved to in progress");
  }
}
