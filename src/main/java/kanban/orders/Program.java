package kanban.orders;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    String keyMenu;
    Task task = new Task();
    do {
      logger.warn("   1 - Add Task" + "   2 - Add to do   ");
      logger.warn("   3 - Add to in progress " + "   4 - Add to code review ");
      logger.warn("   5 - Add to done ");
      logger.warn("   M - Get Task Map" + "   Q - exit");
      logger.warn("   Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        switch (keyMenu) {
          case "1":
            task.addTask();
            break;
          case "2":
            task.toDo();
            break;
          case "3":
            task.inProgress();
            break;
          case "4":
            task.toCodeReview();
            break;
          case "5":
            task.toDone();
            break;
          case "M":
            task.getTaskMap();
            break;
        }
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
