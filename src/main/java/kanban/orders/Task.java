package kanban.orders;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import kanban.orders.state.impl.NewProjectState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task implements State {

  private int taskNumber;
  private static Logger logger = LogManager.getLogger();
  private State state;
  private Tasks tasks;
  private static Scanner input = new Scanner(System.in);

  public Task() {
    state = new NewProjectState();
    tasks = new Tasks();
  }

  public void setState(State state) {
    this.state = state;
  }

  public void addTask() {
    state.addTask(this);
  }

  public void toDo() {
    logger.warn("Choose what task you need: ");
    taskNumber = input.nextInt();
    String taskKey = findTaskByNumber();
    if ((Integer) tasks.getTask().get(taskKey) == 0 ||
        (Integer) tasks.getTask().get(taskKey) == 2) {
      state.addToDo(this);
      tasks.getTask().put(taskKey, 1);
    } else {
      State.super.addToDo(null);
    }

  }

  public void inProgress() {
    logger.warn("Choose what task you need: ");
    taskNumber = input.nextInt();
    String taskKey = findTaskByNumber();
    if ((Integer) tasks.getTask().get(taskKey) == 1 ||
        (Integer) tasks.getTask().get(taskKey) == 3) {
      state.addToInProgress(this);
      tasks.getTask().put(taskKey, 2);
    } else {
      State.super.addToInProgress(null);
    }
  }

  public void toCodeReview() {
    logger.warn("Choose what task you need: ");
    taskNumber = input.nextInt();
    String taskKey = findTaskByNumber();
    if ((Integer) tasks.getTask().get(taskKey) == 2) {
      state.addToCodeReview(this);
      tasks.getTask().put(taskKey, 3);
    } else {
      State.super.addToCodeReview(null);
    }
  }

  public void toDone() {
    logger.warn("Choose what task you need: ");
    taskNumber = input.nextInt();
    String taskKey = findTaskByNumber();
    if ((Integer) tasks.getTask().get(taskKey) == 3) {
      state.addToDone(this);
      tasks.getTask().put(taskKey, 4);
    } else {
      State.super.addToDone(null);
    }

  }

  public void addTaskToMap() {
    logger.debug("Print new task: ");
    String newTask = input.nextLine();
    tasks.setTask(newTask);
  }

  public void getTaskMap() {
    logger.warn(tasks);
  }

  public String findTaskByNumber() {
    taskNumber--;
    List keys = new ArrayList(tasks.getTask().keySet());
    for (int i = 0; i < keys.size(); i++) {
      if (taskNumber == i) {
        return (String) keys.get(i);
      }
    }
    return null;
  }
}
