package kanban.orders;

import java.util.LinkedHashMap;

public class Tasks {
  private LinkedHashMap<String, Integer> taskMap = new LinkedHashMap<>();

  public Tasks() {
    taskMap.put("Do lalal", 0);
    taskMap.put("Do lalalal1", 3);
    taskMap.put("Do lalla2", 0);
  }

  public void setTask(String task) {
    taskMap.put(task, 0);
  }

  public LinkedHashMap getTask() {
    return taskMap;
  }

  @Override
  public String toString() {
    return taskMap.toString();
  }
}
