package kanban.orders;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {

  Logger logger = LogManager.getLogger();

  default void addTask(Task task) {
    logger.warn("Adding task is not allowed");
  }

  default void addToDo(Task task) {
    logger.warn("Adding task in to do is not allowed");
  }

  default void addToInProgress(Task task) {
    logger.warn("Adding task to in progress is not allowed");
  }

  default void addToCodeReview(Task task) {
    logger.warn("Adding task to code review is not allowed");
  }

  default void addToDone(Task task) {
    logger.warn("Add task to done is not allowed");
  }
}
